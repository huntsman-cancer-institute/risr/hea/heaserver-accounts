# When updating package versions, also update the versions of pytest, pytest-xdist, moto, and mypy in HEA Main in the
# heaserver-accounts Dockerfile.
setuptools~=75.2.0
-e .
pytest~=8.3.3
twine~=5.1.1
testcontainers~=4.8.2
mypy==1.11.1
pip-licenses~=5.0.0
aiohttp-swagger3~=0.9.0
moto[s3,sts,organizations]~=5.0.22
pytest-xdist[psutil]~=3.6.1
pytest-cov~=5.0.0
build~=1.2.2.post1
pygount~=1.8.0
pytest-timeout~=2.3.1
